const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = {
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'bundle.js',
    publicPath: '/build/'
  },
  stats: {
    colors: true,
    modules: true,
    assets: true,
    children: true,
    chunks: true,
    depth: true,
    entrypoints: true,
    providedExports: true,
    usedExports: true,
    timings: true,
    hash: true,
    maxModules: 0,
    errors: true,
    warnings: true
  },
  module: {
    rules: [{
      test: /\.s?css$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader'
        },{
          loader: 'sass-loader'
        }]
      })
    }]
  },
  plugins: [
    new ExtractTextPlugin("styles.css")
  ]
};
